# How to move Centreon to another network

## Context
My network address changed and I needed to still be able to supervise using Centreon. The address plan stayed the same from one network to the other and I didn't want to install a new Centreon from scratch & redo the setup of all the hosts and services. This is what I did to achieve the move from one network to the other.

## Topology
The previous network will be referred to as `previous network` and the new one as `new network`. For the sake of this How-to, let's say that on the `previous network` the network address was `192.168.1.0` and the `new network`'s address is `192.168.2.0`.

I had one central `Centreon-Central` (or even `central`) and one `Centreon-Poller` (or `poller`). This is how I'll refer to these VMs here. We'll say that the  `central` address ends with `.2` on both the `previous network` and the `new network`, and the `poller`'s ends with `.3`.

So we have something that looks like this:
|                 | previous network |   new network  |
|:---------------:|:----------------:|:--------------:|
| network address | 192.168.1.0/24   | 192.168.2.0/24 |
| central address | 192.168.1.2      | 192.168.2.2    |
|  poller address | 192.168.1.3      | 192.168.2.3    |

## Methodology
### Create a backup
This is the important thing here, do a backup of each of your VMs in case something goes wrong.

### Change the system's settings on `central` and `poller`
Both `central` and `poller` are on CentOs, so their configuration is very similar. You'll need to do this on both VMs.

First, check what network interface is used (`ip a`) then edit it's configuration. For me, it was `ens160`, so the file to edit was `/etc/sysconfig/network-scripts/ifcfg-ens160`. There, you'll need to modify these fields: `IPADDR`, `GATEWAY`, `DNS1`, `DNS2` and `DOMAIN`, if applicable. They need to apply to the `new network`'s configuration. Then you can restart the network service using `service network restart`.

You'll also need to edit your NTP configuration from the file located here `/etc/ntp.conf`.

If the names of your VMs also changed, you need to modify `/etc/hosts` to reflect the `new network`'s nomenclature. You'll also need to execute the following command `hostnamectl set-hostname new_hostname` then reboot.

Normally, here you'll be able to access Centreon's web interface from `central`'s address on the `new network`, e.g. for me it is `192.168.2.2`.

### Change Centreon's settings from Centreon's database on `central`
Here, I'd recommend to stop every service used by Centreon except the database: `systemctl stop cbd centengine gorgoned snmptrapd snmpd`
From the database, you'll need to edit the values in several tables that are linked to Centreon's own configuration. These are:
- `cfg_centreonbroker_info`,
- `cfg_nagios`,
- `cfg_centreonbroker`,
- `cfg_nagios_broker_module`,
- `nagios_server`,
- `platform_topology`,
- `auth_ressource`,
- `auth_ressource_info`
- `auth_ressource_host`.

Be careful not to forget anything as it can cause problems later on and you'll have to go through the whole list again. Remember to also change the names in paths if you require it.

### Change Hosts names and address from Centreon's database on `central`
Still from the database, you'll need to edit the `hosts` table to reflect the `new network`'s nomenclature and addresses.

### Reboot or restart the services on `central`
Now you can reboot or restart the services we previously stopped. And you're done.
